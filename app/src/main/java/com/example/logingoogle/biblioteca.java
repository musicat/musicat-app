package com.example.logingoogle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class biblioteca extends AppCompatActivity {

    private RequestQueue queue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biblioteca);

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_music:
                        Intent intent1 = new Intent(biblioteca.this, biblioteca.class);
                        startActivity(intent1);
                        //Toast.makeText(Main2Activity.this, "Biblioteca", Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.action_favorites:
                        Intent intent = new Intent(biblioteca.this, CancionesFavoritas.class);
                        startActivity(intent);
                        //Toast.makeText(Main2Activity.this, "Canciones Favoritas", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.action_inicio:
                        //Toast.makeText(Main2Activity.this, "Inicio", Toast.LENGTH_SHORT).show();
                        //break;
                        Intent intent2 = new Intent(biblioteca.this, Main2Activity.class);
                        startActivity(intent2);
                        break;
                }
                return true;
            }
        });




        final ListView CancionesBiblioteca = (ListView)findViewById(R.id.CancionesBiblioteca);
        final ArrayList<String> listaBiblioteca;
        listaBiblioteca = new ArrayList<>();
        queue = Volley.newRequestQueue(biblioteca.this);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listaBiblioteca);
        CancionesBiblioteca.setAdapter(adapter);
        //obtenerDatosVolley();
        final String url = "http://10.0.2.2:3000/canciones/";

        try {
            URLEncoder.encode(url, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        /*JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    //JSONArray mJsonArray = response.getJSONArray("canciones");
                    //String data = response.toString();

                    for(int i=0; i<mJsonArray.length(); i++){
                        JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                        String name = mJsonObject.getString("nombre");
                        adapter.notifyDataSetChanged();
                        //listaBiblioteca = new ArrayList<String>();
                        listaBiblioteca.add(name);
                    }*/

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET,url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try{
                    //JSONArray json = new JSONArray(response);
                    for(int i=0; i<response.length(); i++){
                        //HashMap<String, String> map = new HashMap<String, String>();
                        JSONObject mJsonObject = response.getJSONObject(i);
                        String name = mJsonObject.getString("nombre");
                        adapter.notifyDataSetChanged();
                        //listaBiblioteca = new ArrayList<String>();
                        listaBiblioteca.add(name);
                    }

            } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(request);


    }

    /*private void obtenerDatosVolley(){

        String url = "http://localhost:3000/biblioteca";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    JSONArray mJsonArray = response.getJSONArray("canciones");

                    for(int i=0; i<mJsonArray.length(); i++){
                        JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                        String name = mJsonObject.getString("nombre");
                        //listaBiblioteca = new ArrayList<String>();
                        //listaBiblioteca.add(name);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(request);
    }*/



}
