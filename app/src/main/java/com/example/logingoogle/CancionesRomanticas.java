package com.example.logingoogle;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class CancionesRomanticas extends AppCompatActivity {

    ListView listaCanciones;
    List<String> list;
    ListAdapter adapter;

    MediaPlayer mp;

    int posicion = 0;
    Button play_pause, btn_repetir;

    public String generateUrl(String s){
        String[] p=s.split("/");
        String imageLink="https://drive.google.com/file/d/1Dnpvav7UmVZsU4Wj3s0SjdJ-QUYS7Jy4/view?usp=sharing"+p[5];
        return imageLink;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_canciones_romanticas);

        play_pause = (Button)findViewById(R.id.play);
        listaCanciones = findViewById(R.id.listaCanciones);

        list = new ArrayList<>();

        Field[] fields = R.raw.class.getFields();
        for (int i = 0; i < fields.length; i++){
            list.add(fields[i].getName());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
        listaCanciones.setAdapter(adapter);


        listaCanciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(mp != null ){

                    mp.stop();
                    mp.release();
                }

                int resID = getResources().getIdentifier(list.get(i), "raw", getPackageName());
                mp = MediaPlayer.create(CancionesRomanticas.this, resID);
                mp.start();

                play_pause.setBackgroundResource(R.drawable.ic_launcher_foreground);

            }
        });
        //final ArrayList<String> listaCancion;
        //final ListView lista = (ListView)findViewById(R.id.listaCancionesFav);
        //final String[] listasRep = {"", "", "", ""};



        //listaCancion = new ArrayList<String>();
        //listaCancion.add("All you need is love");
        //listaCancion.add("Sway");
        //listaCancion.add("La vie en rose");
        //listaCancion.add("The Dance");
        //listaCancion.add("Marry you");


        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listaCancion);
        //lista.setAdapter(adapter);


        //lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            //@Override
            //public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

              //  Toast.makeText(CancionesRomanticas.this, "Has pulsado: "+ listaCancion.get(position), Toast.LENGTH_LONG).show();
            //}
        //});
    }

    public void play_pause(View view){
        if (mp.isPlaying()){
            mp.pause();
            play_pause.setBackgroundResource(R.drawable.ic_launcher_foreground);

        }
        else {
            mp.start();
            play_pause.setBackgroundResource(R.drawable.ic_launcher_background);

        }
    }
}
